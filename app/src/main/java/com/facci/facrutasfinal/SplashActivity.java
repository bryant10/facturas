package com.facci.facrutasfinal;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

public class SplashActivity extends AppCompatActivity {

    static int TIMEOUT_MILLIS = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //Se quita la action bar de la pantalla Splash
                getSupportActionBar().hide();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                            //Inicializa la pantalla
                            startActivity(intent);
                            //Finaliza esta activity
                            finish();
                    }
                }, TIMEOUT_MILLIS);
        }
    }
